/********PloyBase Script for classicmodel*****/

drop EXTERNAL TABLE classicmodels.Customers_External;

CREATE EXTERNAL TABLE classicmodels.Customers_External
(
    customerNumber INT,
    customerName VARCHAR(100),
    contactLastName VARCHAR(50),
    contactFirstName VARCHAR(50),
    phone VARCHAR(100),
    addressLine1 VARCHAR(100),
    addressLine2 VARCHAR(100),
    city VARCHAR(100),
    state VARCHAR(100),
	postalCode INT,
	country VARCHAR(100),
	salesRepEmployeeNumber INT,
    creditLimit FLOAT
)
WITH
(
    LOCATION = '/classicmodels/customers/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 2
);

truncate table classicmodels.Customers;

insert into classicmodels.Customers
select * 
 from classicmodels.Customers_External;

drop EXTERNAL TABLE classicmodels.Employees_External;

CREATE EXTERNAL TABLE classicmodels.Employees_External
(
    employeeNumber INT NOT NULL,
    lastName VARCHAR(100),
    firstName VARCHAR(100),
    extension VARCHAR(20),
	email VARCHAR(200),
	officeCode INT,
	reportsTo INT,
	jobTitle  VARCHAR(100)
)
WITH
(
    LOCATION = '/classicmodels/employees/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into classicmodels.Employees
select * 
 from classicmodels.Employees_External;
 
drop EXTERNAL TABLE classicmodels.Offices_External;

CREATE EXTERNAL TABLE classicmodels.Offices_External
(
    officeCode INT NOT NULL ,
    city VARCHAR(50) NOT NULL,
    phone VARCHAR(30),
    addressLine1 VARCHAR(200),
    addressLine2 VARCHAR(200),
    state VARCHAR(100),
    country VARCHAR(100),
    PostalCode VARCHAR(100),
    territory VARCHAR(15)
)

WITH
(
    LOCATION = '/classicmodels/offices/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into classicmodels.Offices
select * from classicmodels.Offices_External;

drop EXTERNAL TABLE classicmodels.Payments_External;

CREATE EXTERNAL TABLE classicmodels.Payments_External
(
    ustomerNumber INT NOT NULL ,
    checkNumber VARCHAR(50) NOT NULL,
    paymentDate DATE,
    amount FLOAT
)
WITH
(
    LOCATION = '/classicmodels/payments/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into classicmodels.Payments
select *  from classicmodels.Payments_External;

drop EXTERNAL TABLE classicmodels.Productlines_External;

CREATE EXTERNAL TABLE classicmodels.Productlines_External
(
    productLine VARCHAR(100),
    textDescription VARCHAR(7500),
    htmlDescription VARCHAR(7500)
)
WITH
(
    LOCATION = '/classicmodels/productlines/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into classicmodels.productlines
select * from classicmodels.Productlines_External;

DROP EXTERNAL TABLE classicmodels.Products_External;

CREATE EXTERNAL TABLE classicmodels.Products_External
(
    productCode VARCHAR(50),
    productName VARCHAR(200),
    productLine VARCHAR(100),
    productScale VARCHAR(10),
	productVendor VARCHAR(200),
	productDescription VARCHAR(1000),
	quantityInStock DECIMAL(10, 2),
	buyPrice DECIMAL(10, 2),
	MSRP DECIMAL(10, 2)
)
WITH
(
    LOCATION = '/classicmodels/products/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 10
);
 
insert into classicmodels.products
select * 
 from classicmodels.Products_External;

