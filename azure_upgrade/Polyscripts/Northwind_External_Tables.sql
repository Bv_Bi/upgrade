/*****************PloyBase Script for northwind******/

CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'Nisum_azure' ;

--drop DATABASE SCOPED CREDENTIAL scoped_credential;

CREATE DATABASE SCOPED CREDENTIAL scoped_credential 
WITH IDENTITY = 'SHARED ACCESS SIGNATURE',
SECRET = 'sJ1ZPk5xAo9tgSNWKAgV43/Ev3Svwy7G6ccObL3E8sk6zdEfgMVZs74c//+f0SNH8eFZ+CG5N6aXltZgusBwqg==';

--drop EXTERNAL DATA SOURCE external_source

CREATE EXTERNAL DATA SOURCE external_source
    WITH ( 
        TYPE = HADOOP,
        LOCATION = 'wasb://talend123@pocbi.blob.core.windows.net',
		CREDENTIAL = scoped_credential
    );

CREATE EXTERNAL FILE FORMAT fileformat_csv
WITH (
    FORMAT_TYPE = DELIMITEDTEXT,
    FORMAT_OPTIONS ( 
        FIELD_TERMINATOR = ',',
        STRING_DELIMITER = '"',
        DATE_FORMAT = '',
        USE_TYPE_DEFAULT = False
    )
);

/**************************External tables****************/

CREATE EXTERNAL TABLE northwind.Categories_External (
    CategoryID INT,
    CategoryName VARCHAR(15) ,
    Description varchar(1000)
)WITH
(
    LOCATION = '/northwind/categories/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Categories
select * 
from northwind.Categories_External;

drop EXTERNAL TABLE northwind.CustomerDemographics_External;

CREATE EXTERNAL TABLE northwind.CustomerDemographics_External (
    CustomerTypeID VARCHAR(10) ,
    CustomerDesc VARCHAR(1000)
)WITH
(
    LOCATION = '/northwind/customerdemographics/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.CustomerDemographics
select * 
from northwind.CustomerDemographics_External;

drop EXTERNAL TABLE northwind.Customers_External;

CREATE EXTERNAL TABLE northwind.Customers_External (
    CustomerID VARCHAR(5) ,
    CompanyName VARCHAR(40) ,
    ContactName VARCHAR(30),
    ContactTitle VARCHAR(30),
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    Phone VARCHAR(24),
    Fax VARCHAR(24)
)WITH
(
    LOCATION = '/northwind/customers/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Customers
select * 
from northwind.Customers_External;

DROP EXTERNAL TABLE northwind.Employee_External;

CREATE EXTERNAL TABLE northwind.Employee_External (
    EmployeeID INT,
    LastName VARCHAR(20),
    FirstName VARCHAR(20),
    Title VARCHAR(30),
    TitleOfCourtesy VARCHAR(25),
    BirthDate datetime2,
    HireDate datetime2,
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    HomePhone VARCHAR(24),
    Extension VARCHAR(10),
    Notes VARCHAR(1000) ,
    ReportsTo NVARCHAR(1000),
    Salary VARCHAR(100)
)WITH
(
    LOCATION = '/northwind/employees/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 10
);

insert into northwind.Employees
select * 
from northwind.Employee_External;

drop EXTERNAL TABLE northwind.EmployeeTerritories_External;

CREATE EXTERNAL TABLE northwind.EmployeeTerritories_External (
    EmployeeID INT ,
    TerritoryID VARCHAR(20) 
)WITH
(
    LOCATION = '/northwind/employeeterritories/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.EmployeeTerritories
select * 
from northwind.EmployeeTerritories_External;

DROP EXTERNAL TABLE northwind.Products_External;

CREATE EXTERNAL TABLE northwind.Products_External (
    ProductID INT  ,
    ProductName VARCHAR(40) ,
    SupplierID INT,
    CategoryID INT,
    QuantityPerUnit VARCHAR(20),
    UnitPrice DECIMAL(10,4),
    UnitsInStock SMALLINT,
    UnitsOnOrder SMALLINT,
    ReorderLevel SMALLINT,
    Discontinued INT
)WITH
(
    LOCATION = '/northwind/products/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Products
select * 
from northwind.Products_External;

DROP EXTERNAL TABLE northwind.Region_External;

CREATE EXTERNAL TABLE northwind.Region_External (
    RegionID INT ,
    RegionDescription VARCHAR(50) 
)WITH
(
    LOCATION = '/northwind/region/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Region
select * 
from northwind.Region_External;

DROP EXTERNAL TABLE northwind.Shippers_External;

CREATE EXTERNAL TABLE northwind.Shippers_External (
    ShipperID INT  ,
    CompanyName VARCHAR(40) ,
    Phone VARCHAR(24)
)WITH
(
    LOCATION = '/northwind/shippers/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Shippers
select * 
from northwind.Shippers_External;

DROP EXTERNAL TABLE northwind.Suppliers_External;

CREATE EXTERNAL TABLE northwind.Suppliers_External (
    SupplierID INT ,
    CompanyName VARCHAR(40) ,
    ContactName VARCHAR(30),
    ContactTitle VARCHAR(30),
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    Phone VARCHAR(24),
    Fax VARCHAR(24),
    HomePage VARCHAR(1000)
)WITH
(
    LOCATION = '/northwind/suppliers/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 20
);

insert into northwind.Suppliers
select * 
from northwind.Suppliers_External;

DROP EXTERNAL TABLE northwind.Territories_External;

CREATE EXTERNAL TABLE northwind.Territories_External (
    TerritoryID VARCHAR(20) ,
    TerritoryDescription VARCHAR(50) ,
    RegionID INT 
)WITH
(
    LOCATION = '/northwind/territories/',
    DATA_SOURCE = external_source,
    FILE_FORMAT = fileformat_csv,
    REJECT_TYPE = value,
    REJECT_VALUE = 1
);

insert into northwind.Territories
select * 
from northwind.Territories_External;

