---Schemas
create schema northwind;
create schema classicmodels;

/******Below tables are being created for northwind schema 
--northwind.Categories
--northwind.CustomerDemographics
--northwind.Customers
--northwind.Employees
--northwind.EmployeeTerritories
--northwind.Products
--northwind.Region
--northwind.Shippers
--northwind.Suppliers
--northwind.Territories
***/

DROP TABLE northwind.Categories;

CREATE TABLE northwind.Categories (
    CategoryID INT NOT NULL,
    CategoryName VARCHAR(15) NOT NULL,
    Description varchar(1000)
)
WITH
(
	CLUSTERED INDEX (CategoryID)
);

DROP TABLE northwind.CustomerDemographics;

CREATE TABLE northwind.CustomerDemographics (
    CustomerTypeID VARCHAR(10) NOT NULL,
    CustomerDesc VARCHAR(1000)
)
WITH
(
	CLUSTERED INDEX (CustomerTypeID)
);

DROP TABLE northwind.Customers;

CREATE TABLE northwind.Customers (
    CustomerID VARCHAR(5) NOT NULL,
    CompanyName VARCHAR(40) NOT NULL,
    ContactName VARCHAR(30),
    ContactTitle VARCHAR(30),
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    Phone VARCHAR(24),
    Fax VARCHAR(24)
)
WITH
(
	CLUSTERED INDEX (CustomerID)
);

DROP TABLE northwind.Employees;

CREATE TABLE northwind.Employees (
    EmployeeID INT NOT NULL,
    LastName VARCHAR(20) NOT NULL,
    FirstName VARCHAR(10) NOT NULL,
    Title VARCHAR(30),
    TitleOfCourtesy VARCHAR(25),
    BirthDate DATETIME2,
    HireDate DATETIME2,
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    HomePhone VARCHAR(24),
    Extension VARCHAR(4),
    Notes VARCHAR(1000) NOT NULL,
    ReportsTo INT,
    Salary FLOAT
)
WITH
(
	CLUSTERED INDEX (EmployeeID)
);

DROP TABLE northwind.EmployeeTerritories;

CREATE TABLE northwind.EmployeeTerritories (
    EmployeeID INT NOT NULL,
    TerritoryID VARCHAR(20) NOT NULL
)
WITH
(
	CLUSTERED INDEX (EmployeeID, TerritoryID)
);

DROP TABLE northwind.Products;

CREATE TABLE northwind.Products (
    ProductID INT NOT NULL,
    ProductName VARCHAR(40) NOT NULL,
    SupplierID INT,
    CategoryID INT,
    QuantityPerUnit VARCHAR(20),
    UnitPrice DECIMAL(10,4) DEFAULT 0,
    UnitsInStock SMALLINT DEFAULT 0,
    UnitsOnOrder SMALLINT DEFAULT 0,
    ReorderLevel SMALLINT DEFAULT 0,
    Discontinued BIT NOT NULL DEFAULT 0
)
WITH
(
	CLUSTERED INDEX (ProductID)
);

DROP TABLE northwind.Region;

CREATE TABLE northwind.Region (
    RegionID INT NOT NULL,
    RegionDescription VARCHAR(50) NOT NULL
)
WITH
(
	CLUSTERED INDEX (RegionID)
);

DROP TABLE northwind.Shippers;

CREATE TABLE northwind.Shippers (
    ShipperID INT NOT NULL,
    CompanyName VARCHAR(40) NOT NULL,
    Phone VARCHAR(24)
)
WITH
(
	CLUSTERED INDEX (ShipperID)
);

DROP TABLE northwind.Suppliers;

CREATE TABLE northwind.Suppliers (
    SupplierID INT NOT NULL,
    CompanyName VARCHAR(40) NOT NULL,
    ContactName VARCHAR(30),
    ContactTitle VARCHAR(30),
    Address VARCHAR(60),
    City VARCHAR(15),
    Region VARCHAR(15),
    PostalCode VARCHAR(10),
    Country VARCHAR(15),
    Phone VARCHAR(24),
    Fax VARCHAR(24),
    HomePage VARCHAR(1000)
)
WITH
(
	CLUSTERED INDEX (SupplierID)
);

DROP TABLE northwind.Territories;

CREATE TABLE northwind.Territories (
    TerritoryID VARCHAR(20) NOT NULL,
    TerritoryDescription VARCHAR(50) NOT NULL,
    RegionID INT NOT NULL
)
WITH
(
	CLUSTERED INDEX (TerritoryID)
);

DROP TABLE northwind.OrderDetails;

CREATE TABLE northwind.OrderDetails (
    OrderID INT NOT NULL,
    ProductID INT NOT NULL,
    UnitPrice FLOAT NOT NULL DEFAULT 0,
    Quantity SMALLINT NOT NULL DEFAULT 1,
    Discount DECIMAL(8, 0) NOT NULL DEFAULT 0
)
WITH
(
	CLUSTERED INDEX (OrderID, ProductID)
);

drop table northwind.Orders;

CREATE TABLE northwind.Orders (
    OrderID INT NOT NULL,
    CustomerID VARCHAR(5),
    EmployeeID INT,
    OrderDate DATETIME,
    RequiredDate DATETIME,
    ShippedDate DATETIME,
    ShipVia INT,
    Freight DECIMAL(10,4) DEFAULT 0,
    ShipName VARCHAR(40),
    ShipAddress VARCHAR(60),
    ShipCity VARCHAR(15),
    ShipRegion VARCHAR(15),
    ShipPostalCode VARCHAR(10),
    ShipCountry VARCHAR(15)
)
WITH
(
	CLUSTERED INDEX (OrderID)
);

/******Below tables are being created for classicmodel db  

--classicmodels.Customers
--classicmodels.Employees
--classicmodels.Offices
--classicmodels.Payments
--classicmodels.ProductLines
--classicmodels.Products

**/
DROP TABLE classicmodels.Customers;

CREATE TABLE classicmodels.Customers (
    customerNumber INT NOT NULL,
    customerName VARCHAR(100) NOT NULL,
    contactLastName VARCHAR(50),
    contactFirstName VARCHAR(50),
    phone VARCHAR(100),
    addressLine1 VARCHAR(100),
    addressLine2 VARCHAR(100),
    city VARCHAR(100),
    state VARCHAR(100),
	postalCode VARCHAR(20),
	country VARCHAR(100),
	salesRepEmployeeNumber INT,
    creditLimit FLOAT
) 
WITH
(
	CLUSTERED INDEX (customerNumber)
);

DROP TABLE classicmodels.Employees;

CREATE TABLE classicmodels.Employees (
    employeeNumber INT NOT NULL,
    lastName VARCHAR(100),
    firstName VARCHAR(100),
    extension VARCHAR(20),
	email VARCHAR(200),
	officeCode INT,
	reportsTo INT,
	jobTitle  VARCHAR(100)
)
WITH
(
	CLUSTERED INDEX (employeeNumber)
);

DROP TABLE classicmodels.Offices;

CREATE TABLE classicmodels.Offices (
    officeCode INT NOT NULL,
    city VARCHAR(50) NOT NULL,
    phone VARCHAR(30),
    addressLine1 VARCHAR(200),
    addressLine2 VARCHAR(200),
    state VARCHAR(100),
    country VARCHAR(100),
    PostalCode VARCHAR(100),
    territory VARCHAR(15)
)
WITH
(
	CLUSTERED INDEX (officeCode)
);

DROP TABLE classicmodels.Payments;

CREATE TABLE classicmodels.Payments (
    customerNumber INT NOT NULL,
    checkNumber VARCHAR(50) NOT NULL,
    paymentDate DATE,
    amount FLOAT
)
WITH
(
	CLUSTERED INDEX (customerNumber,checkNumber)
);

DROP TABLE classicmodels.ProductLines;

CREATE TABLE classicmodels.ProductLines (
    productLine VARCHAR(100) NOT NULL,
    textDescription VARCHAR(1000),
    htmlDescription VARCHAR(1000)
)
WITH
(
	CLUSTERED INDEX (productLine)
);

DROP TABLE classicmodels.Products;

CREATE TABLE classicmodels.Products (
    productCode VARCHAR(50) NOT NULL,
    productName VARCHAR(200),
    productLine VARCHAR(100),
    Productscale VARCHAR(10),
	productVendor VARCHAR(200),
	productDescription VARCHAR(2000),
	quantityInStock DECIMAL(10, 2),
	buyPrice DECIMAL(10, 2),
	MSRP DECIMAL(10, 2)
)
WITH
(
	CLUSTERED INDEX (productCode)
<<<<<<< HEAD
);

---dbo TABLES

DROP TABLE dbo.OrderDetails;

CREATE TABLE dbo.OrderDetails (
    OrderID INT NOT NULL,
    ProductID INT NOT NULL,
    UnitPrice FLOAT NOT NULL DEFAULT 0,
    Quantity SMALLINT NOT NULL DEFAULT 1,
    Discount float NOT NULL DEFAULT 0
)
WITH
(
	CLUSTERED INDEX (OrderID, ProductID)
);

drop table dbo.Orders;

CREATE TABLE dbo.Orders (
    OrderID INT NOT NULL,
    CustomerID VARCHAR(5),
    EmployeeID INT,
    OrderDate DATETIME,
    RequiredDate DATETIME,
    ShippedDate DATETIME,
    ShipVia INT,
    Freight DECIMAL(10,4) DEFAULT 0,
    ShipName VARCHAR(40),
    ShipAddress VARCHAR(60),
    ShipCity VARCHAR(15),
    ShipRegion VARCHAR(15),
    ShipPostalCode VARCHAR(10),
    ShipCountry VARCHAR(15)
)
WITH
(
	CLUSTERED INDEX (OrderID)
);
=======
);
>>>>>>> ff36ff6999b71615d892a5cb22fa1a6336232d3d
