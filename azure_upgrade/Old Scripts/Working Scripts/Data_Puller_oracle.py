import sys
import mysql.connector
import csv
import time
import ConfigParser
import os, errno
import cx_Oracle
#from mysql.connector import errorcode

config = ConfigParser.ConfigParser()
config.read(os.getcwd()+"/config.cnf")

usr=config.get("part1_azure","user")
pwd=config.get("part1_azure","password")
hst=config.get("part1_azure","host")
db=config.get("part1_azure","database")

database=sys.argv[1]
tablename=sys.argv[2]
query=sys.argv[3]
path=sys.argv[4]
eod=sys.argv[5]

path_final=path+'//'+tablename+'//'
timestr = time.strftime("%Y%m%d%H%M")

try:
  if not os.path.exists(path_final):
     os.makedirs(path_final)
except OSError as e:
  if e.errno != errno.EEXIST:
      raise

try:
  db = cx_Oracle.connect('classicmodels/nisum@localhost:1521/orcl')
  cursor = db.cursor()
  filename=path_final+database+'_'+tablename+'_'+timestr+'00'+'.csv'

# execute SQL query using execute() method,

  cursor.execute(query)

# Getting the Column Names

  field_names = [i[0] for i in cursor.description]
  column_names = ','.join(str(e) for e in field_names)
  
  result=cursor.fetchall()

  fp = open(filename,'wb')
  #myFile = csv.writer(fp, lineterminator = '\n')
  myFile = csv.writer(fp)
  myFile.writerow(field_names)
  myFile.writerows(result)
  fp.close()
  db.close()
except :
    print "done"
'''except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)'''

#else:
  #db.close()
