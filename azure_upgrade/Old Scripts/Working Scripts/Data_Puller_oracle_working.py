import sys
import mysql.connector
import csv
import time
import ConfigParser
import os, errno
from mysql.connector import errorcode


class testDataPuller():
    def __init__(self):
        """ make a server connection """
        print ("making a server connection")

    def readConfigFile(self):
        """ make a server connection """
        config = ConfigParser.ConfigParser()
        config.read(os.getcwd()+"/config.cnf")

        usr=config.get("part1_azure","user")
        pwd=config.get("part1_azure","password")
        hst=config.get("part1_azure","host")
        db=config.get("part1_azure","database")
        print ("getting user data")
        database=sys.argv[1]
        tablename=sys.argv[2]
        query=sys.argv[3]
        path=sys.argv[4]
        eod=sys.argv[5]

        return usr, pwd, hst, db, database, tablename, query, path, eod

    def makeDBConnection(self, tablename, path):
        path_final = path + '//' + tablename + '//'
        timestr = time.strftime("%Y%m%d%H%M")
        print ("checking the DB connection")
        try:
          if not os.path.exists(path_final):
             os.makedirs(path_final)
        except OSError as e:
          if e.errno != errno.EEXIST:
              raise
        return path_final, timestr

    def MySQLDB(self, usr, pwd, hst, db, database, tablename, query, path_final, timestr):
        print("querying MySQL DB")
        try:
            db = mysql.connector.connect(user=usr, password=pwd, host=hst, database=db)

            """ execute an actual query """
            self.queryDB(db, database, tablename, query, path_final, timestr)

        except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
          elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
          else:
            print(err)
        else:
          db.close()

        pass

    def queryDB(self, db, database, tablename, query, path_final, timestr):
        print ("querying DB")
        cursor = db.cursor()
        filename = path_final + database + '_' + tablename + '_' + timestr + '00' + '.csv'
        print("connection successful, executing queries")

        """ execute an actual query """
        # execute SQL query using execute() method,

        cursor.execute(query)

        # Getting the Column Names

        field_names = [i[0] for i in cursor.description]
        column_names = ','.join(str(e) for e in field_names)

        result=cursor.fetchall()

        fp = open(filename,'wb')
        # myFile = csv.writer(fp, lineterminator = '\n')
        myFile = csv.writer(fp)
        myFile.writerow(field_names)
        myFile.writerows(result)
        fp.close()
        db.close()
        pass

    def process(self):
        usr, pwd, hst, db, database, tablename, query, path, eod = self.readConfigFile()
        path_final, timestr = self.makeDBConnection(tablename, path)
        self.MySQLDB(usr, pwd, hst, db, database, tablename, query, path_final, timestr)

        pass


if __name__ == "__main__":
    c = testDataPuller()
    c.process()
