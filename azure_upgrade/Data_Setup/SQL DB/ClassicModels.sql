CREATE SCHEMA classicmodels;

CREATE TABLE classicmodels.Customers (
    customerNumber INT NOT NULL,
    customerName VARCHAR(100) NOT NULL,
    contactLastName VARCHAR(50),
    contactFirstName VARCHAR(50),
    phone VARCHAR(100),
    addressLine1 VARCHAR(100),
    addressLine2 VARCHAR(100),
    city VARCHAR(100),
    [state] VARCHAR(100),
  postalCode VARCHAR(20),
  country VARCHAR(100),
  salesRepEmployeeNumber INT,
    creditLimit FLOAT,
  TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE [classicmodels].employees (
  [employeeNumber] int NOT NULL,
  [lastName] varchar(50) NOT NULL,
  [firstName] varchar(50) NOT NULL,
  [extension] varchar(10) NOT NULL,
  [email] varchar(100) NOT NULL,
  [officeCode] varchar(10) NOT NULL,
  [reportsTo] int DEFAULT NULL,
  [jobTitle] varchar(50) NOT NULL,
  TimeUploaded DateTime2 DEFAULT(GetDate()));

  CREATE TABLE [classicmodels].offices (
  [officeCode] varchar(10) NOT NULL,
  [city] varchar(50) NOT NULL,
  [phone] varchar(50) NOT NULL,
  [addressLine1] varchar(50) NOT NULL,
  [addressLine2] varchar(50) DEFAULT NULL,
  [state] varchar(50) DEFAULT NULL,
  [country] varchar(50) NOT NULL,
  [postalCode] varchar(15) NOT NULL,
  [territory] varchar(10) NOT NULL,
  TimeUploaded DateTime2 DEFAULT(GetDate())
  );

CREATE TABLE [classicmodels].payments (
  [customerNumber] int NOT NULL,
  [checkNumber] varchar(50) NOT NULL,
  [paymentDate] date NOT NULL,
  [amount] decimal(10,2) NOT NULL,
  TimeUploaded DateTime2 DEFAULT(GetDate()),
  );

CREATE TABLE [classicmodels].productlines (
  [productLine] varchar(50) NOT NULL,
  [textDescription] varchar(4000) DEFAULT NULL,
  [htmlDescription] varchar(max),
  [image] varbinary(max),
  TimeUploaded DateTime2 DEFAULT(GetDate()),
  );

  CREATE TABLE [classicmodels].products (
  [productCode] varchar(15) NOT NULL,
  [productName] varchar(70) NOT NULL,
  [productLine] varchar(50) NOT NULL,
  [productScale] varchar(10) NOT NULL,
  [productVendor] varchar(50) NOT NULL,
  [productDescription] varchar(max) NOT NULL,
  [quantityInStock] smallint NOT NULL,
  [buyPrice] decimal(10,2) NOT NULL,
  [MSRP] decimal(10,2) NOT NULL,
  TimeUploaded DateTime2 DEFAULT(GetDate())
  );
