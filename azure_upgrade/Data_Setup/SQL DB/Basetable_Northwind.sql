CREATE SCHEMA basetablenorthwind;

CREATE TABLE basetablenorthwind.CustomerDemographics (
    [CustomerTypeID] VARCHAR(10) NOT NULL,
    [CustomerDesc] VARCHAR(max),
	TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Customers (
    [CustomerID] VARCHAR(5) NOT NULL,
    [CompanyName] VARCHAR(40) NOT NULL,
    [ContactName] VARCHAR(30),
    [ContactTitle] VARCHAR(30),
    [Address] VARCHAR(60),
    [City] VARCHAR(15),
    [Region] VARCHAR(15),
    [PostalCode] VARCHAR(10),
    [Country] VARCHAR(15),
    [Phone] VARCHAR(24),
    [Fax] VARCHAR(24),
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Employees (
    [EmployeeID] INTEGER NOT NULL,
    [LastName] VARCHAR(20) NOT NULL,
    [FirstName] VARCHAR(10) NOT NULL,
    [Title] VARCHAR(30),
    [TitleOfCourtesy] VARCHAR(25),
    [BirthDate] DATETIME2(0),
    [HireDate] DATETIME2(0),
    [Address] VARCHAR(60),
    [City] VARCHAR(15),
    [Region] VARCHAR(15),
    [PostalCode] VARCHAR(10),
    [Country] VARCHAR(15),
    [HomePhone] VARCHAR(24),
    [Extension] VARCHAR(4),
    [Notes] VARCHAR(max) NOT NULL,
    [ReportsTo] INTEGER,
     [Salary] FLOAT,
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.EmployeeTerritories (
    [EmployeeID] INTEGER NOT NULL,
    [TerritoryID] VARCHAR(20) NOT NULL,
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Products (
    [ProductID] INTEGER NOT NULL IDENTITY,
    [ProductName] VARCHAR(40) NOT NULL,
    [SupplierID] INTEGER,
    [CategoryID] INTEGER,
    [QuantityPerUnit] VARCHAR(20),
    [UnitPrice] DECIMAL(10,4) DEFAULT 0,
    [UnitsInStock] SMALLINT DEFAULT 0,
    [UnitsOnOrder] SMALLINT DEFAULT 0,
    [ReorderLevel] SMALLINT DEFAULT 0,
    [Discontinued] BINARY NOT NULL DEFAULT 0,
	 TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Region (
    [RegionID] INTEGER NOT NULL,
    [RegionDescription] VARCHAR(50) NOT NULL,
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Suppliers (
    [SupplierID] INTEGER NOT NULL IDENTITY,
    [CompanyName] VARCHAR(40) NOT NULL,
    [ContactName] VARCHAR(30),
    [ContactTitle] VARCHAR(30),
    [Address] VARCHAR(60),
    [City] VARCHAR(15),
    [Region] VARCHAR(15),
    [PostalCode] VARCHAR(10),
    [Country] VARCHAR(15),
    [Phone] VARCHAR(24),
    [Fax] VARCHAR(24),
    [HomePage] VARCHAR(max),
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Shippers (
    [ShipperID] INTEGER NOT NULL IDENTITY,
    [CompanyName] VARCHAR(40) NOT NULL,
    [Phone] VARCHAR(24),
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);

CREATE TABLE basetablenorthwind.Territories (
    [TerritoryID] VARCHAR(20) NOT NULL,
    [TerritoryDescription] VARCHAR(50) NOT NULL,
    [RegionID] INTEGER NOT NULL,
    TimeUploaded DateTime2 DEFAULT(GetDate()),
);