 create view Basetable_CM_offices as 
  select n.[officeCode],n.[city],n.[phone],n.[addressLine1],n.[addressLine2],n.[state],n.[country],n.[postalCode],n.[territory],n.[TimeUploaded] from 
  (select *,row_number() over(partition by officeCode order by TimeUploaded DESC) rn from classicmodels.offices ) n where n.rn = 1;

  select * from Basetable_CM_offices;

   create view Basetable_CM_customers as 
   SELECT n.[customerNumber]
      ,n.[customerName]
      ,n.[contactLastName]
      ,n.[contactFirstName]
      ,n.[phone]
      ,n.[addressLine1]
      ,n.[addressLine2]
      ,n.[city]
      ,n.[state]
      ,n.[postalCode]
      ,n.[country]
      ,n.[salesRepEmployeeNumber]
      ,n.[creditLimit]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by customerNumber order by TimeUploaded DESC) rn from [classicmodels].[Customers]) n where n.rn = 1;

  select * from Basetable_CM_customers;


   create view Basetable_CM_employees as 
SELECT n.[employeeNumber]
      ,n.[lastName]
      ,n.[firstName]
      ,n.[extension]
      ,n.[email]
      ,n.[officeCode]
      ,n.[reportsTo]
      ,n.[jobTitle]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [employeeNumber] order by TimeUploaded DESC) rn from [classicmodels].employees) n where n.rn = 1;

  select * from Basetable_CM_employees


create view Basetable_CM_payments as
SELECT n.[customerNumber]
      ,n.[checkNumber]
      ,n.[paymentDate]
      ,n.[amount]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [customerNumber] order by TimeUploaded DESC) rn from [classicmodels].payments) n where n.rn = 1;

  select * from Basetable_CM_payments

create view Basetable_CM_productlines as
SELECT n.[productLine]
      ,n.[textDescription]
      ,n.[htmlDescription]
      ,n.[image]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [productLine] order by TimeUploaded DESC) rn from [classicmodels].productlines) n where n.rn = 1;


  select * from Basetable_CM_productlines


 create view Basetable_CM_products as
SELECT n.[productCode]
      ,n.[productName]
      ,n.[productLine]
      ,n.[productScale]
      ,n.[productVendor]
      ,n.[productDescription]
      ,n.[quantityInStock]
      ,n.[buyPrice]
      ,n.[MSRP]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [productCode] order by TimeUploaded DESC) rn from [classicmodels].products) n where n.rn = 1;

  select * from Basetable_CM_products




