
create view Basetable_NW_territories as
SELECT n.[TerritoryID]
      ,n.[TerritoryDescription]
      ,n.[RegionID]
      ,n.[TimeUploaded]
	  FROM (select *,row_number() over(partition by [TerritoryID] order by TimeUploaded DESC) rn from [northwind].[Territories]) n where n.rn = 1;

select * from Basetable_NW_territories



create view Basetable_NW_suppliers as
SELECT n.[SupplierID]
      ,n.[CompanyName]
      ,n.[ContactName]
      ,n.[ContactTitle]
      ,n.[Address]
      ,n.[City]
      ,n.[Region]
      ,n.[PostalCode]
      ,n.[Country]
      ,n.[Phone]
      ,n.[Fax]
      ,n.[HomePage]
      ,[TimeUploaded]
  FROM  (select *,row_number() over(partition by [SupplierID] order by TimeUploaded DESC) rn from [northwind].Suppliers) n where n.rn = 1;


  select * from Basetable_NW_suppliers


create view Basetable_NW_shippers as
SELECT n.[ShipperID]
      ,n.[CompanyName]
      ,n.[Phone]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [ShipperID] order by TimeUploaded DESC) rn from [northwind].shippers) n where n.rn = 1;

  select * from Basetable_NW_shippers

  create view Basetable_NW_region as
SELECT n.[RegionID]
      ,n.[RegionDescription]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [RegionID] order by TimeUploaded DESC) rn from [northwind].region) n where n.rn = 1;

  select * from Basetable_NW_region

    create view Basetable_NW_products as
SELECT n.[ProductID]
      ,n.[ProductName]
      ,n.[SupplierID]
      ,n.[CategoryID]
      ,n.[QuantityPerUnit]
      ,n.[UnitPrice]
      ,n.[UnitsInStock]
      ,n.[UnitsOnOrder]
      ,n.[ReorderLevel]
      ,n.[Discontinued]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [ProductID] order by TimeUploaded DESC) rn from [northwind].products) n where n.rn = 1;


  select * from Basetable_NW_products

 create view Basetable_NW_employeeterritories as
SELECT n.[EmployeeID]
      ,n.[TerritoryID]
      ,n.[TimeUploaded]
FROM (select *,row_number() over(partition by [EmployeeID] order by TimeUploaded DESC) rn from [northwind].employeeterritories) n where n.rn = 1;

select * from Basetable_NW_employeeterritories

create view Basetable_NW_employees as
SELECT n.[EmployeeID]
      ,n.[LastName]
      ,n.[FirstName]
      ,n.[Title]
      ,n.[TitleOfCourtesy]
      ,n.[BirthDate]
      ,n.[HireDate]
      ,n.[Address]
      ,n.[City]
      ,n.[Region]
      ,n.[PostalCode]
      ,n.[Country]
      ,n.[HomePhone]
      ,n.[Extension]
      ,n.[Notes]
      ,n.[ReportsTo]
      ,n.[Salary]
      ,n.[TimeUploaded]
	  FROM (select *,row_number() over(partition by [EmployeeID] order by TimeUploaded DESC) rn from [northwind].employees) n where n.rn = 1;

	  select * from Basetable_NW_employees

create view Basetable_NW_customers as
SELECT n.[CustomerID]
      ,n.[CompanyName]
      ,n.[ContactName]
      ,n.[ContactTitle]
      ,n.[Address]
      ,n.[City]
      ,n.[Region]
      ,n.[PostalCode]
      ,n.[Country]
      ,n.[Phone]
      ,n.[Fax]
      ,n.[TimeUploaded]
	  FROM (select *,row_number() over(partition by [CustomerID] order by TimeUploaded DESC) rn from [northwind].customers) n where n.rn = 1;


	  select * from Basetable_NW_customers


create view Basetable_NW_customerdemographics as
SELECT n.[CustomerTypeID]
      ,n.[CustomerDesc]
      ,n.[TimeUploaded]
	  FROM (select *,row_number() over(partition by [CustomerTypeID] order by TimeUploaded DESC) rn from [northwind].customerdemographics) n where n.rn = 1;

	select * from Basetable_NW_customerdemographics


create view Basetable_NW_categories as
SELECT n.[CategoryID]
      ,n.[CategoryName]
      ,n.[Description]
      ,n.[TimeUploaded]
  FROM (select *,row_number() over(partition by [CategoryID] order by TimeUploaded DESC) rn from [northwind].categories) n where n.rn = 1;

  select * from Basetable_NW_categories