import sys
import psycopg2
import csv
import time
import os, errno


class testDataPuller():
    def __init__(self):
        """ make a server connection """
        print ("making a server connection")

    def readConfigFile(self):
        print ("getting user data")
        database=sys.argv[1]
        tablename=sys.argv[2]
        query=sys.argv[3]
        path=sys.argv[4]
        usr=sys.argv[5]
        pwd=sys.argv[6]
        hst=sys.argv[7]
        prt=sys.argv[8]
        tag=sys.argv[9]
        eod=sys.argv[10]

        return database, tablename, query, path, usr, pwd, hst, prt, tag, eod

    def makeDBConnection(self, tablename, path):
        path_final = path + '//' + tablename + '//'
        timestr = time.strftime("%Y%m%d%H%M")
        print ("checking the DB connection")
        try:
          if not os.path.exists(path_final):
             os.makedirs(path_final)
        except OSError as e:
          if e.errno != errno.EEXIST:
              raise
        return path_final, timestr

    def PostgreSQLDB(self, usr, pwd, hst, tag, database, tablename, query, path_final, timestr):
        print("querying PostgreSQLDB")
        try:
            
            db = psycopg2.connect("dbname="+tag+" user="+usr+" host="+hst+" password="+pwd)

            """ execute an actual query """
            self.queryDB(db, database, tablename, query, path_final, timestr)
        except:
            pass
            
        pass

    def queryDB(self, db, database, tablename, query, path_final, timestr):
        print ("querying DB")
        cursor = db.cursor()
        filename = path_final + database + '_' + tablename + '_' + timestr + '00' + '.csv'
        print("connection successful, executing queries")

        """ execute an actual query """
        # execute SQL query using execute() method,

        cursor.execute(query)

        # Getting the Column Names

        field_names = [i[0] for i in cursor.description]
        column_names = ','.join(str(e) for e in field_names)

        result=cursor.fetchall()

        fp = open(filename,'wb')
        # myFile = csv.writer(fp, lineterminator = '\n')
        myFile = csv.writer(fp)
        myFile.writerow(field_names)
        myFile.writerows(result)
        fp.close()
        db.close()
        pass

    def process(self):
        database, tablename, query, path,usr, pwd, hst, prt, tag, eod = self.readConfigFile()
        path_final, timestr = self.makeDBConnection(tablename, path)
        self.PostgreSQLDB(usr, pwd, hst, tag, database, tablename, query, path_final, timestr)

        pass


if __name__ == "__main__":
    c = testDataPuller()
    c.process()
